FROM python:3.9-buster

# RUN apt-get update && apt-get install -y --no-install-recommends librdkafka-dev librdkafka1

COPY . /app
WORKDIR /app

RUN pip install -r requirements.txt

CMD python run.py
