import logging
import asyncio
from starlette.middleware.cors import CORSMiddleware
from fastapi import FastAPI
from app.api.router import api_router
from app.services.sql_alchemy import engine, Base
from app.api.mails.send import run_mail
from app.services.sql_alchemy import SessionLocal


logging.basicConfig()
logging.getLogger('sqlalchemy.engine').setLevel(logging.INFO)

Base.metadata.create_all(bind=engine)

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_methods=["*"],
    allow_credentials=True,
    allow_headers=["*"]
)
app.include_router(api_router, prefix="/mailing")


@app.on_event('startup')
async def on_startup():
    print('Starting')
    data_base = SessionLocal()
    loop = asyncio.get_event_loop()
    loop.create_task(run_mail(data_base, 1, 'New'))
    data_base.close()
    data_base = SessionLocal()
    loop.create_task(run_mail(data_base, 10, 'Error'))
    data_base.close()
