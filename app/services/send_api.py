import requests
# from typing import Union
# from app.api.models.message import MessageModel


# async def params_mailing(phone_code: int, tag: Union[str, None] = None):
#     if tag:
#         return {"phone_code": phone_code, "tag": tag}
#     #return {"phone_code": phone_code}
#     return start_mailing(phone_code)


def external_api(msg_id: int, phone: int, text: str):

    url = f'''https://probe.fbrq.cloud/v1/send/{msg_id}'''
    headers = {
        'user-agent': 'my-app/0.0.1',
        'Content-Type': 'application/json',
        'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJleHAiOjE2OTEwNDcyODUsImlzcyI6ImZhYnJpcXVlIi' +
                         'wibmFtZSI6ImZhZGluX2EifQ.MoMh7cNvRw_rECkYDNHC2W5zDfOuoDtH7X8d2btv37c'
    }
    msg = {'id': msg_id, 'phone': phone, 'text': text}

    response = requests.post(url=url, json=msg, headers=headers)

    return response
