from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import sessionmaker
from app.settigs import settings

SQLALCHEMY_DATABASE_URL = settings.POSTGRES

engine = create_engine(
    SQLALCHEMY_DATABASE_URL
)
SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)

Base = declarative_base()


def get_db():
    data_base = SessionLocal()
    try:
        yield data_base
    finally:
        data_base.close()
