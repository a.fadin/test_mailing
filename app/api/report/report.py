from typing import List
from sqlalchemy.orm import Session
from sqlalchemy import func
from fastapi import Depends
from app.api.models.mail import MailModel
from app.api.models.message import MessageModel
from app.services.sql_alchemy import get_db


def get_report(data_base: Session = Depends(get_db)) -> List:
    res = data_base.query(MailModel.id, MessageModel.status_message, func.count()).join(MessageModel.mailing).\
        group_by(MailModel.id, MessageModel.status_message)
    resp = []
    for row in res:
        resp.append({
            'id_mailing': row[0],
            'msg_status': row[1],
            'count': row[2],
        })
    return resp
