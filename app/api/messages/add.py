from typing import List
from sqlalchemy.orm import Session
from app.api.models.client import ClientModel
from app.api.models.message import MessageModel


def add_messages(clients: List[ClientModel], db_session: Session, mailing_id: int) -> None:
    for client in clients:
        print('Client_1: ', client)
        db_send = MessageModel(
            start_message=None,
            status_message='New',
            id_client=client.id,
            id_mail=mailing_id
        )
        db_session.add(db_send)
