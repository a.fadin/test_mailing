from typing import List, Dict
from fastapi import APIRouter
from app.api.clients.get import get_clients
from app.api.clients.add_client import add_client
from app.api.clients.delete import delete_client
from app.api.clients.patch import path_client
from app.api.mails import add_mail, get_mails, delete_mail, run_mailing, patch_mail
from app.api.report.report import get_report
from app.api.shemas.client import ClientOut
from app.api.shemas.mail import MailSchemeOut
from app.api.shemas.report import ReportSchemeOut


api_router = APIRouter()

api_router.add_api_route(
    path='/clients',
    endpoint=get_clients,
    methods=['GET'],
    tags=['other'],
    response_model=List[ClientOut]
)

api_router.add_api_route(
    path='/clients',
    endpoint=add_client,
    methods=['POST'],
    tags=['other'],
    response_model=ClientOut
)

api_router.add_api_route(
    path='/clients/{id}',
    endpoint=delete_client,
    methods=['DELETE'],
    tags=['other']
)

api_router.add_api_route(
    path='/clients',
    endpoint=path_client,
    methods=['PATCH'],
    tags=['other'],
    response_model=ClientOut
)

api_router.add_api_route(
    path='/mails',
    endpoint=get_mails,
    methods=['GET'],
    tags=['other'],
    response_model=List[MailSchemeOut]
)

api_router.add_api_route(
    path='/mails',
    endpoint=add_mail,
    methods=['POST'],
    tags=['other'],
    response_model=Dict
)

api_router.add_api_route(
    path='/mails/{id}',
    endpoint=delete_mail,
    methods=['DELETE'],
    tags=['other']
)

api_router.add_api_route(
    path='/mails',
    endpoint=patch_mail,
    methods=['PATCH'],
    tags=['other'],
    response_model=MailSchemeOut
)

api_router.add_api_route(
    path='/send',
    endpoint=run_mailing,
    methods=['GET'],
    tags=['other']
)

api_router.add_api_route(
    path='/report',
    endpoint=get_report,
    methods=['GET'],
    tags=['other'],
    response_model=List[ReportSchemeOut]
)
