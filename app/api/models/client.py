from sqlalchemy import Column, Integer, String, BigInteger
from app.services.sql_alchemy import Base


class ClientModel(Base):
    # pylint: disable=too-few-public-methods
    __tablename__ = "clients"

    id = Column(Integer, primary_key=True, index=True)
    phone_number = Column(BigInteger, unique=True)
    phone_code = Column(Integer)
    tag = Column(String)
    timezone = Column(Integer)
