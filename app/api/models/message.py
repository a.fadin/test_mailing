from sqlalchemy import Column, ForeignKey, Integer, String, DateTime
from sqlalchemy.orm import relationship

from app.services.sql_alchemy import Base


class MessageModel(Base):
    # pylint: disable=too-few-public-methods
    __tablename__ = "message"

    id = Column(Integer, primary_key=True, index=True)
    start_message = Column(DateTime)
    status_message = Column(String)
    id_client = Column(Integer, ForeignKey("clients.id", onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    id_mail = Column(Integer, ForeignKey("mails.id", onupdate="CASCADE", ondelete="CASCADE"), nullable=False)
    mailing = relationship("MailModel")
    client = relationship("ClientModel")
