from sqlalchemy import Column, Integer, String, DateTime
from app.services.sql_alchemy import Base


class MailModel(Base):
    # pylint: disable=too-few-public-methods
    __tablename__ = "mails"

    id = Column(Integer, primary_key=True, index=True)
    start_date = Column(DateTime)
    message = Column(String)
    phone_code = Column(Integer)
    tag = Column(String)
    end_date = Column(DateTime)
