from typing import Dict
from sqlalchemy.orm import Session
from fastapi import Depends, HTTPException
from app.api.models.mail import MailModel
from app.services.sql_alchemy import get_db


def delete_mail(db_id: int, data_base: Session = Depends(get_db)) -> Dict:
    delete_mail_from_db(db_session=data_base, mailing_id=db_id)
    data_base.commit()
    return {"Status": "ОК"}


def delete_mail_from_db(db_session: Session, mailing_id: int = -1) -> None:
    db_mail = db_session.query(MailModel).get(mailing_id)
    if db_mail:
        db_session.delete(db_mail)
    else:
        raise HTTPException(status_code=404, detail="Mailing not found")
