from .add import add_mailing_to_db, add_mail
from .get import get_mails
from .send import run_mailing
from .patch import patch_mail
from .delete import delete_mail_from_db, delete_mail


__all__ = [
    'add_mailing_to_db',
    'add_mail',
    'get_mails',
    'run_mailing',
    'patch_mail',
    'delete_mail_from_db',
    'delete_mail'
           ]
