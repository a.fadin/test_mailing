from typing import List
from sqlalchemy.orm import Session
from fastapi import Depends
from app.api.models.mail import MailModel
from app.services.sql_alchemy import get_db


def get_mails(data_base: Session = Depends(get_db)) -> List:
    return data_base.query(MailModel).all()
