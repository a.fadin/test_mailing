from typing import Dict
from datetime import datetime
import asyncio
import requests
from sqlalchemy import func
from sqlalchemy.orm import Session
from sqlalchemy.dialects.postgresql import INTERVAL, TIMESTAMP
from fastapi import Depends
from app.api.models.mail import MailModel
from app.api.models.message import MessageModel
from app.api.models.client import ClientModel
from app.services.sql_alchemy import get_db
from app.services.send_api import external_api


def get_time() -> str:
    time_now = datetime.utcnow().strftime('%Y-%m-%d %H:%M:%S')
    return time_now


def run_mailing(data_base: Session = Depends(get_db), status_='New') -> Dict:
    time_now = get_time()
    resp = {}
    res = data_base.query(MessageModel).join(MessageModel.mailing).join(MessageModel.client).filter(
        MailModel.start_date < func.cast(time_now, TIMESTAMP) + func.cast(
            func.concat(ClientModel.timezone, ' HOURS'), INTERVAL),
        MailModel.end_date > func.cast(time_now, TIMESTAMP) + func.cast(func.concat(ClientModel.timezone, ' HOURS'),
                                                                        INTERVAL),
        MessageModel.status_message == status_).all()

    for message in res:
        status = 'Error'
        print('h')
        try:
            res = external_api(message.id, message.client.phone_number, message.mailing.message)
        except requests.ConnectionError:
            status = 'Error'
        else:
            if res.status_code == 200:
                status = 'Delivered'
        update_message(data_base, status, message.id, time_now)
        resp[message.client.phone_number] = status
    return resp


async def run_mail(data_base: Session, sleep: int, status_='New') -> None:
    while True:
        time_now = get_time()
        resp = {}
        res = data_base.query(MessageModel).join(MessageModel.mailing).join(MessageModel.client).filter(
            MailModel.start_date < func.cast(time_now, TIMESTAMP) + func.cast(
                func.concat(ClientModel.timezone, ' HOURS'), INTERVAL),
            MailModel.end_date > func.cast(time_now, TIMESTAMP) + func.cast(func.concat(ClientModel.timezone, ' HOURS'),
                                                                            INTERVAL),
            MessageModel.status_message == status_).all()

        for message in res:
            status = 'Error'
            print(message.id, message.client.phone_number, message.mailing.message)
            try:
                res = external_api(message.id, message.client.phone_number, message.mailing.message)
            except requests.RequestException:
                status = 'Error'
            else:
                if res.status_code == 200:
                    status = 'Delivered'
            update_message(data_base, status, message.id, time_now)
            resp[message.client.phone_number] = status
        await asyncio.sleep(sleep)


def update_message(data_base, status, id_msg, time_msg_start) -> None:
    msg = data_base.query(MessageModel).get(id_msg)
    msg.status_message = status
    msg.start_message = time_msg_start
    data_base.merge(msg)
    data_base.commit()
    data_base.refresh(msg)
