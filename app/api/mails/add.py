from typing import Union
from sqlalchemy.orm import Session
from fastapi import Depends
from app.api.models.mail import MailModel
from app.services.sql_alchemy import get_db
from app.api.shemas.mail import MailSchemeIn, MailSchemeOut
from app.api.clients.get import get_clients_by_filter
from app.api.messages.add import add_messages


async def add_mail(mail: MailSchemeIn, data_base: Session = Depends(get_db)) -> MailModel:
    db_mail = add_mailing_to_db(data_base=data_base, mail=mail)
    data_base.flush()
    clients = get_clients_by_filter(phone_code=db_mail.phone_code, tag=db_mail.tag, db_session=data_base)
    add_messages(clients=clients, db_session=data_base, mailing_id=db_mail.id)
    data_base.commit()
    return db_mail


def add_mailing_to_db(data_base: Session, mail: Union[MailSchemeIn, MailSchemeOut]) -> MailModel:
    db_mail: MailModel = MailModel(
        start_date=mail.start_date,
        message=mail.message,
        phone_code=mail.phone_code,
        tag=mail.tag,
        end_date=mail.end_date
    )
    data_base.add(db_mail)
    return db_mail
