from sqlalchemy.orm import Session
from fastapi import Depends
from app.services.sql_alchemy import get_db
from app.api.shemas.mail import MailSchemeOut
from app.api.models.mail import MailModel
from .add import add_mailing_to_db
from .delete import delete_mail_from_db


async def patch_mail(mail: MailSchemeOut, data_base: Session = Depends(get_db)) -> MailModel:
    delete_mail_from_db(mailing_id=mail.id, db_session=data_base)
    db_mail = add_mailing_to_db(data_base=data_base, mail=mail)
    data_base.commit()
    return db_mail
