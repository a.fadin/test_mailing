from typing import Dict
from sqlalchemy.orm import Session
from fastapi import Depends
from app.services.sql_alchemy import get_db
from app.api.models.client import ClientModel


def delete_client(id_: int, data_base: Session = Depends(get_db)) -> Dict:
    client = data_base.query(ClientModel).get(id_)
    data_base.delete(client)
    data_base.commit()
    return {"Status": "ОК"}
