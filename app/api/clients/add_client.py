from sqlalchemy.orm import Session
from fastapi import Depends
from app.api.shemas.client import ClientIn, ClientOut
from app.api.models.client import ClientModel
from app.services.sql_alchemy import get_db


async def add_client(client: ClientIn, data_base: Session = Depends(get_db)) -> ClientOut:
    db_user = ClientModel(
        phone_number=client.phone_number,
        phone_code=client.phone_code,
        tag=client.tag,
        timezone=client.timezone
    )
    data_base.add(db_user)
    data_base.commit()
    data_base.refresh(db_user)
    return db_user
