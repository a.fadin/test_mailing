from typing import List
from sqlalchemy.orm import Session
from sqlalchemy import or_
from fastapi import Depends
from app.api.models.client import ClientModel
from app.services.sql_alchemy import get_db
from app.api.shemas.client import ClientOut


def get_clients(data_base: Session = Depends(get_db)) -> List[ClientOut]:

    return data_base.query(ClientModel).all()


def get_clients_by_filter(phone_code: int, tag: str, db_session: Session) -> List[ClientModel]:
    clients = db_session.query(ClientModel).filter(
        or_(phone_code is None, ClientModel.phone_code == phone_code),
        or_(tag is None, ClientModel.tag == tag)
    ).all()
    return clients
