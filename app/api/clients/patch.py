from sqlalchemy.orm import Session
from fastapi import Depends
from app.api.models.client import ClientModel
from app.services.sql_alchemy import get_db
from app.api.shemas.client import ClientOut


async def path_client(client: ClientOut, data_base: Session = Depends(get_db)) -> ClientOut:
    db_client = data_base.query(ClientModel).get(client.id)
    db_client.phone_number = client.phone_number
    db_client.phone_code = client.phone_code
    db_client.tag = client.tag
    db_client.timezone = client.timezone
    data_base.merge(db_client)
    data_base.commit()
    data_base.refresh(db_client)

    return db_client
