from typing import Optional
from datetime import datetime
from .base_scheme import BaseScheme


class MailSchemeOut(BaseScheme):
    # pylint: disable=too-few-public-methods
    id: int
    start_date: datetime
    message: str
    phone_code: Optional[int]
    tag: Optional[str]
    end_date: datetime


class MailSchemeIn(BaseScheme):
    # pylint: disable=too-few-public-methods
    start_date: datetime
    message: str
    phone_code: Optional[int] = None
    tag: Optional[str] = None
    end_date: datetime
