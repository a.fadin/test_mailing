from pydantic import BaseModel


class BaseScheme(BaseModel):
    # pylint: disable=too-few-public-methods
    class Config:
        orm_mode = True
