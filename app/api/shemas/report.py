from .base_scheme import BaseScheme


class ReportSchemeOut(BaseScheme):
    # pylint: disable=too-few-public-methods
    id_mailing: int
    msg_status: str
    count: int
