from typing import Optional
from pydantic import PydanticValueError, validator
from app.api.shemas.base_scheme import BaseScheme


class NotAPhoneError(PydanticValueError):
    # pylint: disable=too-few-public-methods
    code = 'not_a_phone'
    msg_template = 'value is not "phone", got "{wrong_value}"'


class ClientOut(BaseScheme):
    # pylint: disable=too-few-public-methods
    id: Optional[int]
    phone_number: int
    phone_code: int
    tag: Optional[str]
    timezone: int


class ClientIn(BaseScheme):
    # pylint: disable=too-few-public-methods
    phone_number: int
    phone_code: int
    tag: Optional[str]
    timezone: int

    @validator('phone_number')
    def value_must_equal_bar(cls, phone_number) -> str:
        if phone_number <= 70000000000 or phone_number >= 79999999999:
            raise NotAPhoneError(wrong_value=phone_number)
        return phone_number
